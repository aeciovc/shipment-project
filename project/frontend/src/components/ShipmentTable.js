import React, { useEffect } from 'react';
import MaterialTable from 'material-table';

import { makeStyles } from '@material-ui/core/styles';
import SnackbarContent from '@material-ui/core/SnackbarContent';

export default function ShipmentTable(props) {

  const columns = [
    { title: 'Track Number', field: 'track_number', editable: 'onAdd' },
    { title: 'Email', field: 'email_responsible' },
    { title: 'Location', field: 'location' },
    {
      title: 'Carrier',
      field: 'carrier',
      lookup: { 1: 'DHL', 2: 'Fedex', 3: 'USPS', 4: 'USP', 5: 'China Post' }, //TODO It should come from an endpoint
    },
  ]

  const [error, setError] = React.useState({ error: false, msg: 'Error trying to execute this operation. ' });

  const shipmentsData = []
  const [shipments, setShipments] = React.useState(shipmentsData)

  const useStyles = makeStyles(theme => ({
    root: {
      maxWidth: 600,
    },
    snackbar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.error.dark,
    },
  }));

  const classes = useStyles();

  const headers = {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
  };

  useEffect(() => {
    loadData();
  }, [])

  const loadData = () =>{
    fetch(props.endpoint)
      .then(response => {
        if (response.status !== 200) {
          setError( prevState => {
            return { error:true, msg: prevState.msg + response.statusText }
          });
          return setShipments([...shipmentsData])
        }
        return response.json();
      })
      .then(data => {
        setShipments([...data])
      });
  }

  // Process responses
  function processResponse(response) {
    return new Promise((resolve, reject) => {
      let func;
      response.status < 400 ? func = resolve : func = reject;
      response.json().then(data => func({'status': response.status, 'data': data}));
    });
  }

  // Format error response to show on UI
  function formatErrorResponse(obj) {
    return JSON.stringify(obj, null, 2)
  }

  // Add a shipment
  const addShip = newData => {

    fetch(props.endpoint,
    {
      headers: headers,
      method: "POST",
      body: JSON.stringify(newData)
    })
    .then(processResponse)
    .then(response => {
      shipments.push(response.data);
      setShipments([...shipments]);
    })
    .catch(response => {
      setError( prevState => {
        return { error:true, msg: prevState.msg + formatErrorResponse(response.data) }
      });
    });
  }
  
  // Update a shipment
  const updateShip = (newData, oldData) => {
    fetch(`${props.endpoint}${oldData.id}`,
    {
      headers: headers,
      method: "PUT",
      body: JSON.stringify(newData)
    })
    .then(processResponse)
    .then(response => {
      shipments[shipments.indexOf(oldData)] = newData;
      setShipments([...shipments])
    })
    .catch(response => {
      setError( prevState => {
        return { error:true, msg: prevState.msg + formatErrorResponse(response.data) }
      });
    });
  }

  // Remove a shipment
  const removeShip = oldData => {
    fetch(`${props.endpoint}${oldData.id}`,
    {
      headers: headers,
      method: "DELETE"
    })
    .then(response => {
      if (response.status === 204){
        return {};
      } else {
        throw new Error(response.statusText);
      }
    })
    .then(response => {
      shipments.splice(shipments.indexOf(oldData), 1);
      setShipments([...shipments])
    })
    .catch(response => {
      setError( prevState => {
        return { error:true, msg: prevState.msg + formatErrorResponse(response.data) }
      });
    });
  }

  return (
    <div>
    {
      error.error && 
      <SnackbarContent
        className={classes.snackbar}
        message={error.msg}
      />
    }
    <MaterialTable
      title="Shipments List"
      columns={columns}
      data={shipments}
      editable={{
        onRowAdd: newData =>
          new Promise(resolve => {
            setTimeout(() => {
              addShip(newData);
              resolve();
            }, 500);
          }),
        onRowUpdate: (newData, oldData) =>
          new Promise(resolve => {
            setTimeout(() => {
              updateShip(newData, oldData);
              resolve();
            }, 500);
          }),
        onRowDelete: oldData =>
          new Promise(resolve => {
            setTimeout(() => {
              removeShip(oldData);
              resolve();
            }, 500);
          }),
      }}
    />
  </div>
  );
}