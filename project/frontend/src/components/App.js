import React from "react";
import ReactDOM from "react-dom";
import ShipmentTable from "./ShipmentTable";

const App = () => (
  <React.Fragment>
    <ShipmentTable endpoint="api/shipment/"/>
  </React.Fragment>
);

const wrapper = document.getElementById("app");

wrapper ? ReactDOM.render(<App />, wrapper) : null;
