from django.urls import path, include

urlpatterns = [
    path('', include('shipments.urls')),
    path('', include('frontend.urls')),
]