from rest_framework import serializers
from shipments.models import Shipment


class ShipmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Shipment
        fields = '__all__'

    def create(self, validated_data):
        track_number = validated_data.pop('track_number')
        validated_data['track_number'] = track_number.upper()
        shipment = Shipment.objects.create(**validated_data)
        return shipment