from django.db import models

class Shipment(models.Model):

    CARRIER_CHOICES = [
        (1, 'DHL'),
        (2, 'Fedex'),
        (3, 'USPS'),
        (4, 'USP'),
        (5, 'China Post'),
    ]

    created_at = models.DateTimeField(auto_now_add=True)
    track_number = models.CharField(unique=True, max_length=20)
    email_responsible = models.EmailField()
    location = models.CharField(max_length=100)
    carrier = models.IntegerField(
        choices=CARRIER_CHOICES,
        default=1,
    )

    def __str__(self):
        return self.track_number
