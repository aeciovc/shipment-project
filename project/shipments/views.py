from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView

from shipments.models import Shipment
from shipments.serializers import ShipmentSerializer

class ShipmentListCreate(ListCreateAPIView):
    queryset = Shipment.objects.all()
    serializer_class = ShipmentSerializer


class ShipmentGetUpdateDelete(RetrieveUpdateDestroyAPIView):
    queryset = Shipment.objects.all()
    serializer_class = ShipmentSerializer