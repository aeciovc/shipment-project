from django.urls import path
from . import views

urlpatterns = [
    path('api/shipment/', views.ShipmentListCreate.as_view(), name='ShipmentListCreate'),
    path('api/shipment/<int:pk>', views.ShipmentGetUpdateDelete.as_view(), name='ShipmentGetUpdateDelete')
]