import json

from django.test import TestCase, Client
from django.urls import reverse
from rest_framework import status

from .models import Shipment
from .serializers import ShipmentSerializer

#TODO Make more tests
"""
    1- Tests for all endpoints
    2- Tests for edge cases
    3- Unit tests for bussiness layer or model
"""

class CreateShipmentTest(TestCase):
    """ Test module for GET single shipment API """

    def setUp(self):
        # initialize the APIClient app
        self.client = Client()

        self.valid_payload = {
            'track_number':'123456qwert',
            'email_responsible':'aecio@whatever.com',
            'location':'Tallinn - Estonia',
            'carrier':1
        }
        self.invalid_payload = {
            'track_number':'',
            'email_responsible':'',
            'location':'',
            'carrier':1
        }
    
    def test_post_valid_shipment(self):
        response = self.client.post(
            reverse('ShipmentListCreate'), 
            data=json.dumps(self.valid_payload),
            content_type='application/json'
        )
        
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['track_number'], '123456QWERT')
        

    def test_post_invalid_shipment(self):
        response = self.client.post(
            reverse('ShipmentListCreate'), 
            data=json.dumps(self.invalid_payload),
            content_type='application/json'
        )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['email_responsible'][0], 'This field may not be blank.')


class GetShipmentTest(TestCase):
    """ Test module for GET single shipment API """

    def setUp(self):
        # initialize the APIClient app
        self.client = Client()

        self.ship1 = Shipment.objects.create(
            track_number='123456cvbnm',
            email_responsible='aecio@whatever.com',
            location='Tallinn - Estonia',
            carrier=1
        )
        self.ship2 = Shipment.objects.create(
            track_number='123456tyuio',
            email_responsible='aecio@whatever.com',
            location='Tallinn - Estonia',
            carrier=1
        )
    
    def test_get_valid_single_shipment(self):
        response = self.client.get(
            reverse('ShipmentGetUpdateDelete', kwargs={'pk': self.ship1.pk}))
        
        ship1 = Shipment.objects.get(pk=self.ship1.pk)
        serializer = ShipmentSerializer(ship1)

        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_invalid_single_shipment(self):
        response = self.client.get(
            reverse('ShipmentGetUpdateDelete', kwargs={'pk': 30}))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)