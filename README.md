# Shipment project
Backend with Django REST framework / Frontend with React JS

## Setup

# Python
* Create a virtual env: `virtualenv -p python3 <path>`
* Activate the env: `source <path>/bin/activate`
* Install Python dependencies: `pip install -r requirements.txt`
* Migrate: `python ./project/manage.py migrate`
* Populate the database: `python ./project/manage.py loaddata shipment`
* Run locally: `python ./project/manage.py runserver`
* Head over http://127.0.0.1:8000/

# Frontend

* Install Javascript dependencies: `npm i`
* Make the bundle: `npm run dev`

## Test

* API tests: `cd project && python manage.py test`