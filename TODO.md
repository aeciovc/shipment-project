# Improvements and suggestions

## Environment

* Use pipenv as dependencies manager
* Separate front and back end apps

## Tests

* pytest as test framework
* mock services
* coverage
* tests and more kind of tests (unit, e2e, etc.)

## Data base

* I would consider to use a NOSql database

## Docker

* Make containers for api and frontend 